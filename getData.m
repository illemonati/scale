function [data] = getData(auth, user, limit)
    import matlab.net.http.*
    import matlab.net.http.field.*
    import matlab.net.*
    method = RequestMethod.GET;
    date = datestr(datetime('tomorrow'), 'yyyy-mm-dd');
    uri = URI(sprintf("https://web-api.fitbit.com/1.1/user/%s/body/log/weight/list.json?limit=%d&offset=0&sort=desc&beforeDate=%s", user, limit, date));
    authorizationField = AuthorizationField('Authorization', auth);
    header = [authorizationField];
    request = RequestMessage(method, header);
    resp = send(request, uri);
    data = resp.Body.Data.weight;
end
